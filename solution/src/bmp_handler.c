#define _CRT_SECURE_NO_DEPRECATE
#include "bmp_handler.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

enum compression{
    BI_RGB,
    BI_RLE8,
    BI_RLE4,
    BI_BITFIELDS,
    BI_JPEG,
    BI_PNG,
    BI_ALPHABITFIELDS
};


struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t pixXPerMeter;
    uint32_t pixYPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
}__attribute__((packed));

static void print_error_message(const char* format, ...) {
    va_list argptr;
    va_start(argptr, format);

    // https://stackoverflow.com/questions/58672959/why-does-clang-tidy-say-vsnprintf-has-an-uninitialized-va-list-argument
    // NOLINTNEXTLINE
    vfprintf(stderr, format, argptr);

    va_end(argptr);
}

#define print_error(...) \
    fprintf(stderr, "%s in function %s on line %d\n", __FILE__, __FUNCTION__, __LINE__); \
    print_error_message(__VA_ARGS__);

#define BMP_TYPE 19778

static bool open_file_on_read(const char* path, FILE** file){
    *file = fopen(path, "rb");
    if (*file == NULL){
        print_error("unable to open file on read \"%s\"\n", path);
        return false;
    }
    return true;
}


static bool open_file_on_write(const char* path, FILE** file){
    *file = fopen(path, "wb");
    if (*file == NULL){
        print_error("unable to open file on write\"%s\"\n", path);
        return false;
    }
    return true;
}

static bool close_file(FILE* file){
    if (file == NULL){
        print_error("unable to close file because file pointer is NULL\n");
        return false;
    }
    const int i = fclose(file);
    if (i == 0) return true;

        print_error("unable to close file because of some\n");
        return false;
}


static bool read_bmp_header(FILE* file, struct bmp_header* bmp_header){
    const uint64_t amount_of_read_objects = fread(bmp_header, sizeof( struct bmp_header ), 1, file );
    const int8_t return_value = fseek(file, 0, SEEK_SET);
    if (amount_of_read_objects != 1 || return_value != 0) {
        print_error("Failed to read bmp header.\n");
    }
    return (amount_of_read_objects == 1);
}


static bool is_bmp_file(FILE* file){
    if (file == NULL){
        print_error("Unable to determine the file type because file pointer is NULL\n");
        return false;
    }
    const uint16_t code_of_bmp_file = BMP_TYPE;
    uint16_t code_of_type;
    const bool b = fread(&code_of_type, sizeof (uint16_t), 1, file);
    if (fseek(file, 0, SEEK_SET) != 0) return false;
    if (!b){
        print_error("Unable to determine the file type because failed to read the file\n");
        return false;
    }
    if (code_of_bmp_file != code_of_type){
        print_error("Unsupported format with code %" PRIu16 ". The only one that is supported is 0x4D42 (the bmp code)\n", code_of_type);
        return false;
    }
    return true;
}


static bool is_bmp_have_24_color_depth(FILE* file){
    if (file == NULL) return false;
    struct bmp_header header = {0};
    if (!read_bmp_header(file, &header)){
        print_error("Unable to determine color depth of the image because failed to read the header of the file");
        return false;
    } else if (header.biBitCount != 24){
        print_error("Unsupported color depth %"PRIu16 ". The only one that supported is 24.\n", header.biBitCount);
        return false;
    }
    return true;
}

static uint8_t count_padding(uint32_t width){
    return width % 4;; //using uint8_t cos max value of padding is 3
}


static struct bmp_header create_bmp_header(struct image img){
    struct bmp_header bmp_header = {0};
    bmp_header.bfType = BMP_TYPE;
    bmp_header.bfReserved = 0;
    bmp_header.bOffBits = sizeof (struct bmp_header);
    bmp_header.biSize = 40;
    bmp_header.biWidth = img.width;
    bmp_header.biHeight = img.height;
    bmp_header.biPlanes = 1;
    bmp_header.biBitCount = 24;
    bmp_header.biCompression = BI_RGB;
    bmp_header.biSizeImage = img.height * (img.width * 3 + count_padding(img.width));
    bmp_header.bfileSize = bmp_header.bOffBits + bmp_header.biSizeImage;
    bmp_header.pixXPerMeter = 0;
    bmp_header.pixYPerMeter = 0;
    bmp_header.biClrUsed = 0;
    bmp_header.biClrImportant = 0;
    return bmp_header;
}


static bool read_pixel_array(FILE* file, struct pixel** const pixel_array_ptr){
    if (file == NULL || !is_bmp_file(file) || !is_bmp_have_24_color_depth(file)){
        print_error("Unable to read pixel array because some error has occurred");
        return false;
    }

    struct bmp_header bmpHeader = {0};
    if (!read_bmp_header(file, &bmpHeader)){
        return false;
    }

    if (*pixel_array_ptr == NULL) {
        print_error("Unable to read rgb array because failed to allocate %" PRIu32 "bytes of heap memory\n", bmpHeader.biSizeImage);
        return false;
    }
    const uint32_t offset_of_pixel_data = bmpHeader.bOffBits;
    const uint32_t width = bmpHeader.biWidth;
    const uint8_t padding = count_padding(width);

    if (fseek(file, (long) offset_of_pixel_data, SEEK_SET) != 0) return false;

    for (size_t i = 0; i < bmpHeader.biHeight; i++){
        if (fread( *pixel_array_ptr + i * width, sizeof( struct pixel ), width, file ) != width) return false;
        if (fseek(file, padding, SEEK_CUR) != 0) return false;
    }
    if (fseek(file, 0 , SEEK_SET) != 0) return false;
    return true;

}

static bool write_img_data_to_file(FILE* file, struct image img){

    const uint8_t padding = count_padding(img.width);
    const uint8_t trash[] = { 0, 0, 0 };

    for (size_t i = 0; i < img.height; i++){
        if (fwrite( img.data + i * img.width, sizeof( struct pixel ), img.width, file) != img.width ) return false;
        fwrite( trash, sizeof(uint8_t), padding, file);
    }

    if (fseek(file, 0, SEEK_SET) != 0) return false;
    return true;
}


static bool write_bmp_header_to_file(FILE* file, struct bmp_header* bmpHeaderPointer ){
    if (fwrite(bmpHeaderPointer, sizeof (struct bmp_header), 1, file) != 1) return false;
    if (fseek(file, 0, SEEK_SET) != 0) return false;
    return true;
}


bool bmp_to_image(const char* path, struct image* img){
    FILE* file = NULL;
    if (!open_file_on_read(path, &file)) return false;
    if (!is_bmp_file(file) || !is_bmp_have_24_color_depth(file)){
        print_error("Unable to read rgb array because some error has occurred");
        close_file(file);
        return false;
    }
    struct bmp_header header = {0};
    if (!read_bmp_header(file, &header)) {
        close_file(file);
        return false;
    }
    const uint32_t height = header.biHeight;
    const uint32_t  width = header.biWidth;
    img->height = height;
    img->width = width;

    struct pixel* pixels = malloc(header.biSizeImage);
    if (pixels == NULL){
        print_error("Unable to translate bmp to image because failed to allocate %zu bytes of heap memory", header.biSizeImage);
        close_file(file);
        return false;
    }

    if (!read_pixel_array(file, &pixels)) {
        print_error("read_pixel_array() failed");
        if (pixels != NULL) free(pixels);
        return false;
    }
    img->data = pixels;
    return true;
}


bool image_to_bmp(const struct image* img, const char* path){
    FILE* file = NULL;
    if (!open_file_on_write(path, &file)) return false;
    struct bmp_header bmp_header = create_bmp_header(*img);
    if (!write_bmp_header_to_file(file, &bmp_header)) {
        print_error("Unable to write bmp_header to a file because some error has occurred");
        return false;
    }
    if (fseek(file, (long)bmp_header.bOffBits, SEEK_SET) != 0) return false;

    if (!write_img_data_to_file(file, *img)) {
        print_error("write_img_data_to_file() failed");
        close_file(file);
        return false;
    }

    close_file(file);
    return true;
}
