#include "transition_rotate.h"
#include <malloc.h>
#include <stdint.h>


struct image image_create(uint64_t width, uint64_t height, struct pixel* pixels){
    struct image new_image = {.width = width, .height = height, .data = pixels};
    return new_image;
}

struct image rotate_left(const struct image* source_image) {
    const uint64_t source_height = source_image->height;
    const uint64_t source_width = source_image->width;
    struct pixel *new_array = malloc(sizeof(struct pixel) * (source_height * source_width));
    for (uint64_t i = 0; i < source_height; i++) {
        for (uint64_t j = 0; j < source_width; j++) {
            const uint64_t index_of_new_place = (j + 1) * (source_height) - i - 1;
            const uint64_t index_of_old = (i * source_width) + j;
            new_array[index_of_new_place] = source_image->data[index_of_old];
        }
    }
    return image_create(source_height, source_width, new_array);
}


